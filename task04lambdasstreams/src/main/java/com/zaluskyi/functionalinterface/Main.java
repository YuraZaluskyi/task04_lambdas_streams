package com.zaluskyi.functionalinterface;

import com.zaluskyi.functionalinterface.view.View;

public class Main {
    public static void main(String[] args) {
        View myView = new View();
        myView.start();
    }
}
