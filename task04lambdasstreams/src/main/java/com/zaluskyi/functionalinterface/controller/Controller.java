package com.zaluskyi.functionalinterface.controller;

import com.zaluskyi.functionalinterface.model.RealizationManipulationOfNumbers;
import org.apache.logging.log4j.*;


import java.util.InputMismatchException;
import java.util.Scanner;

public class Controller {
    private static Logger logger = LogManager.getLogger(Controller.class);
    private Scanner input = new Scanner(System.in);

    public void findMaxNumber() {
        try {
            logger.info("Enter first integer number...");
            int firstNumber = input.nextInt();
            logger.info("Enter second integer number...");
            int secondNumber = input.nextInt();
            logger.info("Enter third integer number...");
            int thirdNumber = input.nextInt();
            int max = RealizationManipulationOfNumbers.findMaxNumber.manipulationThreeNumbers(firstNumber, secondNumber, thirdNumber);
            logger.info("Max number is {}", max);
        } catch (InputMismatchException e) {
            logger.error("Incorrect number!");
        }


    }

    public void findMinNumber() {
        try {
            logger.info("Enter first integer number...");
            int firstNumber = input.nextInt();
            logger.info("Enter second integer number...");
            int secondNumber = input.nextInt();
            logger.info("Enter third integer number...");
            int thirdNumber = input.nextInt();
            int min = RealizationManipulationOfNumbers.findMinNumber.manipulationThreeNumbers(firstNumber, secondNumber, thirdNumber);
            logger.info("Min number is {}", min);
        } catch (InputMismatchException e) {
            logger.error("Incorrect number!");
        }
    }

    public void averageNumber() {
        try {
            logger.info("Enter first number...");
            int firstNumber = input.nextInt();
            logger.info("Enter second integer number...");
            int secondNumber = input.nextInt();
            logger.info("Enter third integer number...");
            int thirdNumber = input.nextInt();
            int average = RealizationManipulationOfNumbers.averageNumber.manipulationThreeNumbers(firstNumber, secondNumber, thirdNumber);
            logger.info("Average is ... {}", average);
        } catch (InputMismatchException e) {
            logger.error("Incorrect number!");
        }
    }
}
