package com.zaluskyi.functionalinterface.model;

@FunctionalInterface
public interface ManipulationOfNumbers {

    int manipulationThreeNumbers(int a, int b, int c);
}
