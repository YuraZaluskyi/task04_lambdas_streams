package com.zaluskyi.functionalinterface.model;


public class RealizationManipulationOfNumbers {

    public static ManipulationOfNumbers findMaxNumber = (a, b, c) -> a > b ? a : (b > c ? b : c);
    public static ManipulationOfNumbers averageNumber = (a, b, c) -> (a + b + c) / 3;
    public static ManipulationOfNumbers findMinNumber = (a, b, c) -> a < b ? a : (b < c ? b : c);
}
