package com.zaluskyi.functionalinterface.view;

import com.zaluskyi.functionalinterface.controller.Controller;
import org.apache.logging.log4j.*;

import java.util.Scanner;

public class View {
    private static Logger logger = LogManager.getLogger(View.class);
    private Controller controller = new Controller();
    private Scanner input = new Scanner(System.in);

    public View() {
    }

    public void start() {
        while (true) {
            menu();
            String choice = input.next();
            switch (choice) {
                case "1":
                    controller.findMaxNumber();
                    break;
                case "2":
                    controller.findMinNumber();
                    break;
                case "3":
                    controller.averageNumber();
                    break;
                default:
                    return;

            }
        }
    }

    public void menu() {
        logger.info("\n 1 - find max number \n 2 - find min number \n 3 - find average number \n E X I T - press any key");
    }
}
