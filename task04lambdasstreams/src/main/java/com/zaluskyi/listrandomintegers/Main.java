package com.zaluskyi.listrandomintegers;

import com.zaluskyi.listrandomintegers.view.View;

public class Main {
    public static void main(String[] args) {
        View myView = new View();
        myView.start();
    }

}
