package com.zaluskyi.listrandomintegers.controller;

import com.zaluskyi.listrandomintegers.model.RandomNumber;
import org.apache.logging.log4j.*;

import java.util.Scanner;

public class Controller {
    private static Logger logger = LogManager.getLogger(Controller.class);
    private Scanner sc = new Scanner(System.in);
    private RandomNumber randomNumber = new RandomNumber();

    public Controller() {
    }

    private void setStartEndListIntegers() {
        logger.info("Enter start number...");
        int startNumber = sc.nextInt();
        logger.info("Enter end number...");
        int endNumber = sc.nextInt();
        logger.info("Enter quantity numbers...");
        int quantityNumbers = sc.nextInt();
        randomNumber.setStartNumber(startNumber);
        randomNumber.setEndNumber(endNumber);
        randomNumber.setQuantity(quantityNumbers);
    }

    public void createListEvenIntegers() {
        setStartEndListIntegers();
        randomNumber.randomEvenNumbers();
    }

    public void createListOddIntegers() {
        setStartEndListIntegers();
        randomNumber.randomOddNumbers();
    }

    public void findMaxNumber() {
        logger.info("max number is...{} ", randomNumber.findMaxNumber());
    }

    public void findMinNumber() {
        logger.info("min number is...{} ", randomNumber.findMinNumber());
    }

    public void findAverageValue() {
        logger.info("average value is...{}", randomNumber.findAverageValue());
    }

    public void findQuantityElementsMoreThanAverageValue() {
        logger.info("quantity elements more than average value is...{}"
                , randomNumber.findQuantityElementsMoreThanAverageValue());
    }

    public void findSumAllElements() {
        logger.info("sum all elements is...{}", randomNumber.findSumAllElements());
    }
}
