package com.zaluskyi.listrandomintegers.model;

import java.util.*;

public class RandomNumber {
    private int quantity;
    private int startNumber;
    private int endNumber;
    private List<Integer> listIntegers = new ArrayList<>();

    public RandomNumber() {
    }

    public RandomNumber(int quantity, int startNumber, int endNumber) {
        this.quantity = quantity;
        this.startNumber = startNumber;
        this.endNumber = endNumber;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getStartNumber() {
        return startNumber;
    }

    public void setStartNumber(int startNumber) {
        this.startNumber = startNumber;
    }

    public int getEndNumber() {
        return endNumber;
    }

    public void setEndNumber(int endNumber) {
        this.endNumber = endNumber;
    }

    public List<Integer> getListIntegers() {
        return listIntegers;
    }

    public void setListIntegers(List<Integer> listIntegers) {
        this.listIntegers = listIntegers;
    }

    //methods//////////////////////////////////////////////////////////////////////////////////////////////////////

    public int getRandomNumber(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    public void randomEvenNumbers() {
        int randomNumber = 0;
        listIntegers.clear();
        for (int i = 0; i < quantity; i++) {
            randomNumber = getRandomNumber(startNumber, endNumber);
            if (randomNumber % 2 == 0) {
                listIntegers.add(randomNumber);
            }
        }
    }

    public void randomOddNumbers() {
        int randomNumber = 0;
        listIntegers.clear();
        for (int i = 0; i < quantity; i++) {
            randomNumber = getRandomNumber(startNumber, endNumber);
            if (randomNumber % 2 != 0) {
                listIntegers.add(randomNumber);
            }
        }
    }

    public Integer findMaxNumber() {
        return listIntegers.stream().max(Comparator.naturalOrder()).get();
    }

    public Integer findMinNumber() {
        return listIntegers.stream().min(Comparator.naturalOrder()).get();
    }

    public double findAverageValue() {
        return listIntegers.stream().mapToInt(e -> e).average().getAsDouble();
    }

    public long findQuantityElementsMoreThanAverageValue() {
        return listIntegers.stream().filter(e -> e > findAverageValue()).count();
    }

    public long findSumAllElements() {
        return listIntegers.stream().mapToLong(e -> e).sum();

    }

    public String toString() {
        return "RandomNumber{" +
                "quantity=" + quantity +
                '}';
    }
}
