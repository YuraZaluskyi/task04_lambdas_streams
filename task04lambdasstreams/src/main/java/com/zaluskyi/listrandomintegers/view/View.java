package com.zaluskyi.listrandomintegers.view;

import com.zaluskyi.listrandomintegers.controller.Controller;
import org.apache.logging.log4j.*;

import java.util.Scanner;

public class View {
    private static Logger logger = LogManager.getLogger(View.class);
    private Controller controller = new Controller();
    private Scanner input = new Scanner(System.in);

    public View() {
    }

    public void getMessage() {
        logger.info("info message");
    }

    public void start() {
        while (true) {
            menu();
            String choice;
            choice = input.next();
            switch (choice) {
                case "1":
                    controller.createListEvenIntegers();
                    subAction();
                    break;
                case "2":
                    controller.createListOddIntegers();
                    subAction();
                    break;
                default:
                    return;
            }
        }
    }

    private void subAction() {
        while (true) {
            subMenu();
            String choice;
            choice = input.next();
            switch (choice) {
                case "1":
                    controller.findMaxNumber();
                    break;
                case "2":
                    controller.findMinNumber();
                    break;
                case "3":
                    controller.findAverageValue();
                    break;
                case "4":
                    controller.findQuantityElementsMoreThanAverageValue();
                    break;
                case "5":
                    controller.findSumAllElements();
                    break;
                default:
                    return;
            }
        }
    }

    public void menu() {
        logger.info("\n 1 - create a list even integers \n 2 - create a list odd integers" +
                " \n E X I T - press any key");
    }

    public void subMenu() {
        logger.info("\n 1 - find max number \n 2 - find min number " +
                "\n 3 - find average value \n 4 - find quantity elements more than average value" +
                "\n 5 -  find sum all elements" +
                "\n E X I T - press any key");
    }
}
