package com.zaluskyi.patterncommand;

import com.zaluskyi.patterncommand.comp.Comp;
import com.zaluskyi.patterncommand.user.User;

public class Main {
    public static void main(String[] args) {
        Comp comp = new Comp();
        User user = new User();
        user.resetComputer();
        user.startComputer();
        user.stopComputer();

    }
}
