package com.zaluskyi.patterncommand.command;

@FunctionalInterface
public interface Command {
    void execute();
}
