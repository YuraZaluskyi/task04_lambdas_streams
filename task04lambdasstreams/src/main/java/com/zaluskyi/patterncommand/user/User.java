package com.zaluskyi.patterncommand.user;

import com.zaluskyi.patterncommand.command.Command;

public class User {
    Command start = () -> System.out.println("Start computer");
    Command stop = new Command() {
        @Override
        public void execute() {
            System.out.println("Stop computer");
        }
    };
    Command reset = () -> System.out.println("Reset computer");

    public User() {
        this.start = start;
        this.stop = stop;
        this.reset = reset;
    }
    
    public void startComputer() {
        start.execute();
    }

    public void stopComputer() {
        stop.execute();
    }

    public void resetComputer() {
        reset.execute();
    }


}
