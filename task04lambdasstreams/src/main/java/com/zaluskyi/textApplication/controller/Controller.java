package com.zaluskyi.textApplication.controller;

import com.zaluskyi.textApplication.model.CustomerText;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Scanner;

public class Controller {

    private static Logger logger = LogManager.getLogger(Controller.class);
    private Scanner input = new Scanner(System.in);
    private CustomerText customerText = new CustomerText();

    public Controller() {
    }

    public Controller(CustomerText customerText) {
        this.customerText = customerText;
    }

    public CustomerText getCustomerText() {
        return customerText;
    }

    public void setCustomerText(CustomerText customerText) {
        this.customerText = customerText;
    }

    //methods////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void textInput() {
        logger.info("");
        logger.info("Enter text...");
        logger.info("");
        String text = "";
        String textFromOneRow = "";
        do {
            textFromOneRow = input.nextLine();
            text += textFromOneRow + " ";
        } while (!textFromOneRow.isEmpty());
        customerText.setText(text);
    }

    public void findUniqueWords() {
        logger.info(Arrays.toString(customerText.findUniqueWordInText()));
    }

    public void printWords() {
        customerText.findUniqueWordInText();
    }

    public void findNumberRepetitionElements() {
        customerText.findNumberRepetitionElements();
    }


}
