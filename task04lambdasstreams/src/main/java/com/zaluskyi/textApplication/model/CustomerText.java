package com.zaluskyi.textApplication.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.stream.Collectors;

public class CustomerText {
    private static Logger logger = LogManager.getLogger(CustomerText.class);
    private String text;

    public CustomerText() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    //methods/////////////////////////////////////////////////////////////////////////////////////////////////////

    public String[] arrayWords() {
        return text.toLowerCase().trim().split(" ");
    }

    public Object[] findUniqueWordInText() {
        String[] wordArray = text.toLowerCase().trim().split(" ");
        return Arrays.stream(wordArray)
                .sorted()
                .distinct()
                .toArray();
    }

    public void findNumberRepetitionElements() {
        Arrays.stream(arrayWords())
                .collect(Collectors.groupingBy(s -> s))
                .forEach((k, v) -> logger.info(k, " -> ", v.size()));

    }

    @Override
    public String toString() {
        return "CustomerText{" +
                "text='" + text + '\'' +
                '}';
    }
}
