package com.zaluskyi.textApplication.view;

import com.zaluskyi.textApplication.controller.Controller;
import org.apache.logging.log4j.*;

import java.util.Scanner;

public class View {
    private static Logger logger = LogManager.getLogger(View.class);
    private Controller controller = new Controller();
    private Scanner input = new Scanner(System.in);


    public void start() {
        String choice;
        while (true) {
            menu();
            logger.info("make your choice...");
            choice = input.next();
            switch (choice) {
                case "1":
                    controller.textInput();
                    break;
                case "2":
                    controller.findUniqueWords();
                    controller.printWords();
                    break;
                default:
                    return;
            }
        }
    }

    private void menu() {
        logger.info("   M E N U");
        logger.info("");
        logger.info("1 - enter text");
        logger.info("2 - find unique words");
        logger.info("3 - find number repetition elements");
    }
}
